EmailContents::Application.routes.draw do
  resources :users, :only  => [:index,:create,:update,:destroy,:show] do
    resources :contacts, :only => :index
    resources :comments, :only => [:index, :create, :destroy, :update]
  end

  resources :contacts, :only  => [:create,:update,:destroy,:show] do
    resources :comments, :only => [:index, :show]
  end

  resources :contact_shares, :only => [:create, :destroy]
end
