class Contact < ActiveRecord::Base
  validates :user_id,:email,:name,:presence  => true
  belongs_to :user
  has_many :contact_shares
  has_many :shared_users, through: :contact_shares, source: :user

  has_many :comments, as: :commentable
end
