class ContactsController < ApplicationController

  def create
    contact = Contact.new(contact_params)
    if contact.save
      render :json  => contact
    else
      render :json  => contact.errors.full_messages,
             :status => :unprocessable_entity
    end
  end

  def destroy
    Contact.find(params[:id])
    if contact
      contact.destroy
      render :json => "You killed him! (or /her!)"
    else
      render :json => "it doesn't exist"
    end
  end

  def index
    render :json  => contacts_for_user_id(params[:user_id])
  end

  def show
    render :json  => Contact.find(params[:id])
  end

  def update
    contact = Contact.find(params[:id])
    contact.update_attributes(contact_params)
    if contact.save
      render :json  => contact
    else
      render :json  => contact.errors.full_messages,
             :status => :cant_update
    end

  end


  def contacts_for_user_id(id)
    share_cons = Contact.find_by_sql("
    SELECT *
    FROM contacts
    WHERE user_id = #{id} OR contacts.id IN (
                                         SELECT contact_id
                                         FROM contact_shares
                                         WHERE user_id = #{id}
                                          )"
    )
  end
  private
  def contact_params
    params.require(:contact).permit(:name, :email, :user_id)
  end
end
