class UsersController < ApplicationController

  def create
    user = User.new(user_params)
    if user.save
      render :json  => user
    else
      render :json  => user.errors.full_messages,
             :status => :unprocessable_entity
    end
  end

  def destroy
    user = User.find(params[:id])
    if user
      user.destroy
      render :json => "You killed him! (or /her!)"
    else
      render :json => "it doesn't exist"
    end
  end

  def index
    render :json  => User.all
  end

  def show
    render :json  => User.find(params[:id])
  end

  def update
    user = User.find(params[:id])
    user.update_attributes(user_params)
    if user.save
      render :json  => user
    else
      render :json  => user.errors.full_messages,
             :status => :cant_update
    end

  end


  private
  def user_params
    params[:user].permit(:username, :contacts, :contact_shares,                                           :shared_contacts)
  end
end
