class CommentsController < ApplicationController
  def create
    author_id = params[:user_id]
    params = comment_params.merge({:author_id => author_id})
    new_comment = Comment.new(params)

    if new_comment.save
      render :json  => new_comment
    else
      render :json  => new_comment.errors.full_messages, status: :bad_comment
    end
  end

  private
  def comment_params
    params.require(:comments).permit(:author_id, :commentable_type,
                                    :commentable_id, :body)
  end
end
