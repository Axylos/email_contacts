class ContactSharesController < ApplicationController

  def create
    new_share = ContactShare.new(con_share_params)
    if new_share.save!
      render :json => new_share
    else
      render :json  => new_share.errors.full_messages,
             :status => :unprocessable_entity
    end
  end

  def destroy
    contact_share = ContactShare.find(params[:id])
    if contact_share
      contact_share.destroy
      render :json => "#{contact_share} Destroyed!"
    else
      render :json => "it doesn't exist"
    end
  end

  private

  def con_share_params
    params[:contact_share].permit(:user_id, :contact_id)
  end
end
